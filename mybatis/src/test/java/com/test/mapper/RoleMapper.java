package com.test.mapper;

import com.test.po.Role;
import com.test.po.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleMapper {
    Role getRole(@Param("id") Long id, Long id2);

    Role findRole(String roleName);

    int deleteRole(@Param("id") Long id);

    int updateRole(Role role);

    int insertRole(Role role);

    int saveBatch(@Param("entityList") List<Role> entityList);

    List<Role> getRoleList(@Param("ids") List<String> ids);

    List<Role> getRoleListByMap(@Param("idsMap") Map<String,String> ids);

    User getUserByUserId(int userID);

}
