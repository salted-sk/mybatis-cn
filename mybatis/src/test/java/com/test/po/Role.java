package com.test.po;

import java.io.Serializable;
import java.util.List;

/*
 * @author gethin
 * 角色的实体类
 */
public class Role implements Serializable {
    private Long id;
    private String roleName;
    private String note;
    private String userId;

    private User user;

    public Role(){

    }

    public Role(Long id,String roleName,String note){
        this.id = id;
        this.roleName = roleName;
        this.note = note;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
